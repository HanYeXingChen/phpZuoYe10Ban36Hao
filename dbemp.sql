-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2017 年 11 月 06 日 07:46
-- 服务器版本: 5.6.12-log
-- PHP 版本: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `db_emp`
--
CREATE DATABASE IF NOT EXISTS `db_emp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_emp`;

-- --------------------------------------------------------

--
-- 表的结构 `tb_emp`
--

CREATE TABLE IF NOT EXISTS `tb_emp` (
  `e_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `e_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `e_dept` varchar(20) CHARACTER SET utf8 NOT NULL,
  `e_img` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT 'images/1.jpg',
  `date_of_birth` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `date_of_entry` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `tb_emp`
--

INSERT INTO `tb_emp` (`e_id`, `e_name`, `e_dept`, `e_img`, `date_of_birth`, `date_of_entry`) VALUES
(1, '张三', '市场部', 'images/1.jpg', '2008-04-02 21:33:00', '0000-00-00 00:00:00'),
(2, '李四', '开发部', 'images/1.jpg', '2008-04-02 21:33:00', '0000-00-00 00:00:00'),
(3, '王五', '媒体部', 'images/1.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '赵六', '销售部', 'images/1.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '陈天天', '市场部', 'images/1.jpg', '2008-04-02 21:33:00', '0000-00-00 00:00:00'),
(6, '网红', '策划部', 'images/1.jpg', '2008-04-02 21:33:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
