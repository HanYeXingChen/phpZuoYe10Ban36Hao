-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2017 年 12 月 05 日 07:56
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `db_emp`
--

-- --------------------------------------------------------

--
-- 表的结构 `tb_emp`
--

CREATE TABLE IF NOT EXISTS `tb_emp` (
  `e_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `e_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `e_dept` varchar(20) CHARACTER SET utf8 NOT NULL,
  `e_img` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT 'images/1.jpg',
  `date_of_birth` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `date_of_entry` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `tb_emp`
--

INSERT INTO `tb_emp` (`e_id`, `e_name`, `e_dept`, `e_img`, `date_of_birth`, `date_of_entry`) VALUES
(1, '张三', '市场部', 'images/1.jpg', '2008-04-02 21:33:00', '0000-00-00 00:00:00'),
(2, '李四', '开发部', 'images/4.jpg', '2008-04-02 21:33:00', '0000-00-00 00:00:00'),
(3, '王五', '媒体部', 'images/2.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '赵六', '销售部', 'images/3.jpg', '1990-02-01 18:02:00', '2010-06-05 22:06:06'),
(5, '陈上思', '市场部', 'images/1.jpg', '1992-12-04 18:34:45', '2016-02-26 04:12:12'),
(6, '小尾巴', '策划部', 'images/2.jpg', '2017-12-04 18:35:34', '2013-06-05 04:02:02'),
(7, 'CCTV', '开发部', 'images/1.jpg', '1992-08-18 08:33:21', '2008-03-12 08:42:40'),
(8, 'Meng', '媒体部', 'images/1.jpg', '1997-11-26 03:23:31', '2015-10-16 04:31:29'),
(9, 'MC', '策划部', 'images/3.jpg', '1999-06-26 04:45:00', '2008-07-02 04:22:00'),
(10, '风月', '市场部', 'images/4.jpg', '1995-09-22 07:26:00', '2016-12-13 08:27:00'),
(11, '晓梦', '销售部', 'images/3.jpg', '1997-03-28 04:12:00', '2017-12-05 07:35:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
